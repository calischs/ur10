import socket
import sys
from ax12 import *

dyna = Ax12()
servo_min = 452
servo_max = 522

#dyna.learnServos(verbose=False) #to learn what dynamixel motors are connected
#right now this is hard coded for servo id 1.  could change this eventually...

def handle_data(data,connection):
	data = data.split(',')
	while(True):
		try:
			if data[0] == 'm':
				p = int(data[1])
				if (servo_min <= p) and (p <= servo_max):
					dyna.move(1,int(data[1]))
			elif data[0] == 's':
				#there seem to be problems in ax12.py for this...
				connection.sendall("%04d"%dyna.readSpeed(1))
			elif data[0] == 'p':
				connection.sendall("%04d"%dyna.readPosition(1))
			elif data[0] == 't':
				connection.sendall("%04d"%dyna.readTemperature(1))
			elif data[0] == 'l':
				connection.sendall("%04d"%dyna.readLoad(1))
			elif data[0] == 'v':
				connection.sendall("%04d"%dyna.readVoltage(1))
		except(dyna.timeoutError):
			continue
		break


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('192.168.1.55', 10000)
print 'starting up on %s port %s' % server_address
sock.bind(server_address)

sock.listen(1)

while True:
	print 'waiting for a connection'
	connection, client_address = sock.accept()
	print 'connection from', client_address
	try:
	    while True:
			data = connection.recv(6)
			if data:
				handle_data(data,connection)
				#connection.sendall(data)
			else:
				break
	except KeyboardInterrupt:
		break
	finally:
		print "closing socket"
		connection.close()