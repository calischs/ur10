from UR10 import *

#Sam Calisch, 2015
#Center for Bits and Atoms, MIT
	

roy = UR10("Roy","192.168.1.52",30002)
siegfried = UR10("Siegfried","192.168.1.51",30002)
roy.open_socket()
siegfried.open_socket()

last = time.time()
try:
	while(True):
		roy.read_packet()
		siegfried.read_packet()

except(KeyboardInterrupt):
	pass
roy.close_socket()
siegfried.close_socket()
print "Thank you."


