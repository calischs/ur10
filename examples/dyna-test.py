import socket
import sys
from time import sleep

def move(p,sock,delay=1.):
	sock.sendall('m,%04d'%p)
	sleep(delay-.1)
	sock.sendall('p,0000')
	print "Position: %d "%int(sock.recv(4))
	sleep(.1)	


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('192.168.1.55', 10000)
sock.connect(server_address)
'''
test script for dynamixel over tcp/ip
message format: first char gives command type:
	-m: move
	-p: get position
	-s: get speed
	-t: get temperature
	-v: get voltage
first char followed by comma
four digits give a position
'''

try:
	sock.sendall('v,0000')
	print "Voltage: %1f volts"%(float(sock.recv(4))/10)
	sleep(.1)

	sock.sendall('t,0000')
	print "Temperature: %d degrees Celsius"%int(sock.recv(4))
	sleep(.1)

	sock.sendall('p,0000')
	print "Position: %d "%int(sock.recv(4))
	sleep(.1)

	#foot up: 452
	#zero: 473
	#full tilt: 522
	#move(452,sock)
	move(473,sock)
	move(490,sock)
	#move(522,sock)
	move(473,sock)
	move(452,sock)

finally:
    print 'closing socket'
    sock.close()