from UR10 import *
from numpy import *

#Sam Calisch, 2015
#Center for Bits and Atoms, MIT
	

#roy = UR10("Roy","192.168.1.52")
siegfried = UR10("Siegfried","192.168.1.51")
#roy.open_socket()
siegfried.open_socket()

period = 10
t = arange(0,period,.1)
z_traj = .1*sin(2*pi*t/period)
v_traj = 2*.1*2*pi/period*cos(2*pi*t/period)


last = time.time()
for i in range(10): siegfried.read_packet() #establish starting position
start = siegfried.tool_vector
print "start=",start
i = 0
#siegfried.send("set_digital_out(2,False)\n")
try:
	while(True):
		#roy.read_packet()
		siegfried.read_packet()
		new_p = start+array([0,0,z_traj[i],0,0,0])
		new_p.append(.1)		
		#move_cmd = 'movep(p[%f, %f, %f, %f, %f, %f],a=1.2,v=.%f,r=.025)\n'%(tuple(new_p)+(abs(v_traj[i]),))
		#move_cmd = 'movep(p[%f, %f, %f, %f, %f, %f],a=1.2,v=.%f,r=.025)\n'%(tuple(new_p)+tuple([.1]))
		#move_cmd = 'movep(p[%f, %f, %f, %f, %f, %f],a=1.2,v=.05,r=.025)\n'%tuple(new_p)
		print move_cmd		
		siegfried.send(move_cmd)		
		#speed_cmd = 'speedl([%f, %f, %f, 0, 0, 0],a=.2,t_min=.05)\n'%(0,0,z_traj[i])	
		#print speed_cmd
		#siegfried.send(speed_cmd)
		print "[%.4f,%.4f,%.4f,%.4f,%.4f,%.4f]"%tuple(siegfried.tool_vector-start)		
		i = (i+1)%shape(t)[0]
except(KeyboardInterrupt):
	pass
#roy.close_socket()
siegfried.close_socket()
print "Thank you."


