import numpy as np
import cv2
from UR10 import *


roy = UR10("Roy","192.168.1.52")
roy.open_socket(30002)

cap = cv2.VideoCapture(0)
ret, frame = cap.read()
h,w,d = np.shape(frame)


while(True):
	ret, frame = cap.read()
	roy.read_packet_secondary()

	# Our operations on the frame come here
	#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	tv = roy.tool_vector
	if len(tv):
		cv2.putText(frame,"%.4f, %.4f, %.4f"%tuple(tv[:3]),(0,h-40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0),2)
		cv2.putText(frame,"%.4f, %.4f, %.4f"%tuple(tv)[3:],(0,h-10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255),2)
	
	cv2.imshow('frame',frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

cap.release()
cv2.destroyAllWindows()