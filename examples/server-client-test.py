import socket
import argparse

def main(args):
	f = file(args.filename,'r')
	values = [l.strip('\n') for l in f.readlines()]
	#print values
	f.close()
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((args.ip, args.port))

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Serve a set of values from a csv file')
	parser.add_argument('-f','--filename', default="values.csv", help="filename for csv file of values")
	parser.add_argument('-ip','--ip',default="192.168.1.51",help='ip address of robot')	parser.add_argument('-p','--port',default="30002",help='port for tcpip socket')
	
	args = parser.parse_args()
	main(args)
