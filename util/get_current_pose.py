from UR10 import *
from numpy import *
import sys

#Sam Calisch, 2015
#Center for Bits and Atoms, MIT
	
#siegfried = UR10("Siegfried","192.168.1.51")
robot = UR10(sys.argv[1],sys.argv[2])
robot.open_socket(30002)

last = time.time()
for i in range(10): robot.read_packet_secondary() #establish starting position
print "[%f, %f, %f, %f, %f, %f]"%tuple(robot.tool_vector)
robot.close_socket()
print "Thank you."


